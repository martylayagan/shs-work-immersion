from django.shortcuts import render
from django.views.generic import TemplateView

# Create your views here.

class NamePageView(TemplateView):
    template_name = "name.html"


    def get_context_data(self):
        data = {"favQoute" : "God is Good",
                "Habits" : "Playing dota"}
        return data

class ResumePageView(TemplateView):
    template_name = "resume.html"
    def get_context_data(self):
        data = {"shs": "Camarines Sur National high School",
                "jhs": "Camarines Sur National High School",
                "elem": " Tabuco Central School"}
        return data




class InfoPageView(TemplateView):
    template_name = "info.html"

    def get_context_data(self):
        data = {"name": "Raymart L. Layagan",
                "address": "Z-1 Lupi San Fernando Camarines Sur",
                "email": "martylayagan669@gmail.com",
                "contact":"09501318339"}
        return data
