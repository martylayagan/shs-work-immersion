from django.apps import AppConfig


class RampageAppConfig(AppConfig):
    name = 'rampage_app'
