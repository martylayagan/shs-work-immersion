from django.urls import path
from .views import NamePageView, ResumePageView, InfoPageView


urlpatterns = [
    path("name/", NamePageView.as_view(), name="name_view"),
    path("resume/", ResumePageView.as_view(), name="resume_view"),
     path("contact information/", InfoPageView.as_view(), name="info_view")

]